/*
 * Copyright (c) 2019 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#ifndef MAIN_H_
#define MAIN_H_

#define NAME "MZ-X CC Mapper"
#define INPORT_NAME NAME " input"
#define OUTPORT_NAME NAME " output"

extern int verbose;

#endif /* MAIN_H_ */
