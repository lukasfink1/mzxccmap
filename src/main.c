/*
 * Copyright (c) 2019 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "main.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "midi.h"

#define ARGSTR "hvni:c:m:u:"

extern char *optarg;
extern int optind;

int verbose = 0;

static void argparse(int argc, char *argv[]);

static void usage(const char *name)
{	// usage: %s [-h] [-vv] [-n] [-i INPORT] [-c CHANNEL] [-m SRC:DEST]… [-u SRC]… [outport]…
	printf("usage: %s [option]... [outport]...\n\n"
		"Maps midi volume controls from different channels to different controls\n"
		"on a single channel. All other events will be passed through unchanged.\n\n"
		"  outport                subscribe to midi output port outport\n\n"
		"  -u SRC                 unmaps (ignore) volume control from channel SRC\n"
		"                         (can be used multiple times)\n"
		"  -m SRC:DEST            maps volume control from channel SRC to control DEST\n"
		"                         (can be used multiple times)\n"
		"  -c CHANNEL             put modified events on this midi CHANNEL (0-15)\n"
		"  -i INPORT              subscribe to midi input port INPORT\n"
		"                         (can be used multiple times)\n"
		"  -n                     stops automatically subscribing to CASIO ports\n"
		"  -v                     activates verbose mode; use twice to print all events\n"
		"  -h                     prints this help\n", name);
	exit(0);
}

int main(int argc, char *argv[])
{
	char c;
	int err;

	argparse(argc, argv);

	if ((err = seq_init()) < 0) goto alsaerr;

	optind = 1;
	while ((c = getopt(argc, argv, ARGSTR)) != -1)
		if (c == 'i' && (err = seq_inport_subscribe(optarg)) < 0) goto alsaerr;

	for (char **i = argv + optind; *i; i++)
		if ((err = seq_outport_subscribe(*i)) < 0) goto alsaerr;

	mainloop(argv[0]);

	return 0;
alsaerr:
	sperror(argv[0], err);
	return 1;
}

static void argparse(int argc, char *argv[])
{
	char c, *end = NULL, *end2 = NULL;
	unsigned long num;
	unsigned char mapch;

	while ((c = getopt(argc, argv, ARGSTR)) != -1)
	{
		switch (c)
		{
		case 'h':
			usage(argv[0]);
			break;
		case 'v':
			verbose++;
			break;
		case 'n':
			autosubscribe = 0;
			break;
		case 'c':
			num = strtoul(optarg, &end, 0);
			if (*end || num > 15)
			{
				fprintf(stderr, "%s: channel number invalid: %s\n", argv[0], optarg);
				exit(2);
			}
			channel = num;
			break;
		case 'm':
			num = strtoul(optarg, &end, 0);
			if (*end != ':')
			{
				fprintf(stderr, "%s: expected ':' as divider: %s\n", argv[0], optarg);
				exit(2);
			}
			if (num > 15)
			{
				*end = '\0';
				fprintf(stderr, "%s: SRC channel invalid: %s\n", argv[0], optarg);
				exit(2);
			}
			mapch = num;

			num = strtoul(++end, &end2, 0);
			if (*end2 || num > 127)
			{
				fprintf(stderr, "%s: DEST control invalid: %s\n", argv[0], end);
				exit(2);
			}

			map[mapch] = num;
			break;
		case 'u':
			num = strtoul(optarg, &end, 0);
			if (*end || num > 15)
			{
				fprintf(stderr, "%s: SRC channel invalid: %s\n", argv[0], optarg);
				exit(2);
			}
			map[num] = -1;
			break;
		case '?':
			fprintf(stderr, "Try '%s -h' for more information.\n", argv[0]);
			exit(2);
		}
	}
}
