/*
 * Copyright (c) 2019 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#ifndef MIDI_H_
#define MIDI_H_

extern unsigned char map[];
extern unsigned char channel;
extern int autosubscribe;

void sperror(const char *name, int err);
int seq_init(void);
int seq_inport_subscribe(const char *str);
int seq_outport_subscribe(const char *str);
void mainloop(const char *name);

#endif /* MIDI_H_ */
