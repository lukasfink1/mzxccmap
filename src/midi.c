/*
 * Copyright (c) 2019 Lukas Fink
 * Released under the MIT license. Read 'LICENSE' for more information.
 */

#include "midi.h"

#include <stdio.h>
#include <alsa/asoundlib.h>
#include <alloca.h>

#include "main.h"

#define INPORT_CAPS (SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE)
#define OUTPORT_CAPS (SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ)
#define PORT_TYPE (SND_SEQ_PORT_TYPE_PORT | SND_SEQ_PORT_TYPE_SOFTWARE)

unsigned char map[16] = {102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117};
unsigned char channel = 0;
int autosubscribe = 1;

static snd_seq_t *seqh = NULL;
static int inport, outport;

void sperror(const char *name, int err)
{
	fprintf(stderr, "%s: %s\n", name, snd_strerror(err));
}

int seq_init(void)
{
	int err, clientid;

	if ((err = snd_seq_open(&seqh, "default", SND_SEQ_OPEN_DUPLEX, 0)) < 0 ||
		(err = snd_seq_set_client_name(seqh, NAME)) < 0 ||
		(inport = err = snd_seq_create_simple_port(seqh, INPORT_NAME, INPORT_CAPS, PORT_TYPE)) < 0)
		return err;
	clientid = snd_seq_client_id(seqh);
	printf("created midi input port %d:%d\n", clientid, inport);

	if ((outport = err = snd_seq_create_simple_port(seqh, OUTPORT_NAME, OUTPORT_CAPS, PORT_TYPE)) < 0)
		return err;
	printf("created midi output port %d:%d\n", clientid, outport);

	if (autosubscribe && (err = seq_inport_subscribe("CASIO USB-MIDI")) >= 0)
		puts("autosubscribe successful");

	return 0;
}

int seq_inport_subscribe(const char *str)
{
	snd_seq_addr_t addr;
	int err;

	if ((err = snd_seq_parse_address(seqh, &addr, str)) < 0 ||
		(err = snd_seq_connect_from(seqh, inport, addr.client, addr.port)) < 0)
		return err;

	printf("subscribed to sender port %d:%d\n", addr.client, addr.port);
	return 0;
}

int seq_outport_subscribe(const char *str)
{
	snd_seq_addr_t addr;
	int err;

	if ((err = snd_seq_parse_address(seqh, &addr, str)) < 0 ||
		(err = snd_seq_connect_to(seqh, outport, addr.client, addr.port)) < 0)
		return err;

	printf("subscribed to receiver port %d:%d\n", addr.client, addr.port);
	return 0;
}

void mainloop(const char *name)
{
	snd_seq_event_t *ev = NULL;
	unsigned char dest;
	int err;

	while (1)
	{
		if ((err = snd_seq_event_input(seqh, &ev)) < 0)
		{
			sperror(name, err);
			continue;
		}

		if (ev->type == SND_SEQ_EVENT_CONTROLLER && ev->data.control.param == 7)
		{
			dest = map[ev->data.control.channel];
			if (dest == -1) continue;
			ev->data.control.channel = channel;
			ev->data.control.param = dest;
			if (verbose)
				printf("sending modified cc event: channel=%d, control=%d, value=%d\n",
					channel, dest, ev->data.control.value);
		}
		else if (verbose >= 2)
			puts("passing through unmodified event.");

		snd_seq_ev_set_source(ev, outport);
		snd_seq_ev_set_subs(ev);
		snd_seq_ev_set_direct(ev);
		snd_seq_event_output(seqh, ev);
		snd_seq_drain_output(seqh);
	}
}
