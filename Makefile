CC = cc
LD = cc
RM = rm -rf
EXECUTABLE = mzxccmap
LIBS = -lasound
OBJS = ./main.o ./midi.o
CFLAGS = -O2 -Wall -c
LDFLAGS = 

.PHONY: all clean

all: $(EXECUTABLE)

clean:
	$(RM) $(EXECUTABLE) $(OBJS)

$(EXECUTABLE): $(OBJS)
	@echo 'Building target $@'
	$(LD) $(LDFLAGS) -o "$@" $(OBJS) $(LIBS)

%.o: src/%.c
	@echo 'Building file $<'
	$(CC) $(CFLAGS) -o "$@" "$<"

