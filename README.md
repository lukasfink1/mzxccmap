# mzxccmap

The Casio MZ-X300 and MZ-X500 have linear faders primarily used as organ drawbars. When using these keyboards as MIDI controllers, it would be convenient to be able to use these faders, too.
Unfortunately they don’t create CC events in organ mode, but they can be also used as volume controls in the mixer setting, where they generate volume CC events on the corresponding channel.
This program remaps these to different CC events all on a single channel and thus makes it more compatible with different software.
